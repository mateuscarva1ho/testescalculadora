﻿using System;

namespace calculadora
{
    class Program
    {
        static void Main(string[] args)
        {
            Calcular calculadora = new Calcular();

            calculadora.Num1 = double.Parse(Console.ReadLine());
            calculadora.Num2 = double.Parse(Console.ReadLine());
            calculadora.Operacao = Console.ReadLine();

            double resultado = calculadora.ExecutarCalculo();

            Console.WriteLine(resultado);
        }
    }
}
