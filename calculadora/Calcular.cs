﻿using System;
using System.Collections.Generic;
using System.Text;

namespace calculadora
{
    public class Calcular
    {
        public double Num1 { get; set; }
        public double Num2 { get; set; }
        public string Operacao { get; set; }

        public double ExecutarCalculo()
        {
            switch (this.Operacao)
            {

                case "somar":
                    return Num1 + Num2;
                case "subtrair":
                    return Num1 - Num2;
                case "multiplicacao":
                    return Num1 * Num2;
                case "divisao":
                    return Num1 / Num2;
                default:
                    return 0;
            }
            
            //if (this.Operacao == "somar")
            //return this.Num1 + this.Num2;
            //else if (this.Operacao == "subtrair")
            //return this.Num1 - this.Num2;
            //else if (this.Operacao == "multiplicacao")
            //return this.Num1 * this.Num2;
            //else if (this.Operacao == "divisao")
            //return this.Num1 / this.Num2;
            //return 0;

        }
    }
}
